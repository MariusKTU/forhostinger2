import { Component } from '@angular/core';
import { MockServerResultsService } from './paging/mock-server-results-service';
import { CorporateEmployee } from './model/corporate-employee';
import { Page } from './model/page';

@Component({
  selector: 'app-root',
  providers: [MockServerResultsService],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  page = new Page();
  rows = new Array<CorporateEmployee>();
  selected: any[] = [];
  cache: any = {};
  selectedItem: any;
  ifSelected: boolean;


  // ColumnMode = ColumnMode;

  constructor(private serverResultsService: MockServerResultsService) {
    this.page.pageNumber = 0;
  }

  /**
   * Populate the table with new data based on the page number
   * @param page The page to select
   */
  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.page.size = pageInfo.pageSize;

    // cache results
    // if(this.cache[this.page.pageNumber]) return;

    this.serverResultsService.getResults(this.page).subscribe(pagedData => {
      this.page = pagedData.page;

      // calc start
      const start = this.page.pageNumber * this.page.size;

      // copy rows
      const rows = [...this.rows];

      // insert rows into new position
      rows.splice(start, 0, ...pagedData.data);

      // set rows to our new rows
      this.rows = rows;

      // add flag for results
      this.cache[this.page.pageNumber] = true;
    });
  }
  onSelect(event) {
    console.log('Event: select', event, this.selected);
    if (!!event) {
      if (this.selected.length !== 0) {
        this.ifSelected = true;
        this.selectedItem = event.selected[0];
      }
    }
  }

  onActivate(event) {
    console.log('Event: activate', event);
  }
}
